﻿using UnityEngine;
using System.Collections;

public class RaySteer : MonoBehaviour {
    float steerInput = 0;
    RCC_CarControllerV3 carController;
    EliranAI eliranAI;
    bool avoid = false;

    // Raycast distances.
    public LayerMask obstacleLayers = -1;
    public int wideRayLength = 20;
    public int tightRayLength = 20;
    public int sideRayLength = 3;
    private float rayInput = 0f;
    private bool raycasting = false;
    private float resetTime = 0f;
    // Use this for initialization
    void Start () {
	    carController = GetComponent<RCC_CarControllerV3>();
        eliranAI = GetComponent<EliranAI>();
    }
	
	// Update is called once per frame
	void Update () {
        FixedRaycasts();
        if (carController.direction == 1)
        {

            steerInput = Mathf.Clamp(rayInput, -1f, 1f);
        }
        else
        {
            steerInput = Mathf.Clamp((-rayInput), -1f, 1f);
        }

        if (raycasting && Mathf.Abs(rayInput) > 0.3)
        {
            eliranAI.avoid = true;
            carController.steerInput = steerInput;
        }
        else
            eliranAI.avoid = false;
        
       // steerInput = Mathf.Clamp(rayInput, -1f, 1f);
    //    Debug.Log(steerInput + " EnemySpeed: " + carController.speed + " PlayerSpeed: " + eliranAI.pvp.speed );
    }

        void FixedRaycasts()
    {

        Vector3 forward = transform.TransformDirection(new Vector3(0, 0, 1));
        Vector3 pivotPos = new Vector3(transform.localPosition.x, carController.FrontLeftWheelCollider.transform.position.y, transform.localPosition.z);
        RaycastHit hit;

        // New bools effected by fixed raycasts.
        bool tightTurn = false;
        bool wideTurn = false;
        bool sideTurn = false;
        bool tightTurn1 = false;
        bool wideTurn1 = false;
        bool sideTurn1 = false;

        // New input steers effected by fixed raycasts.
        float newinputSteer1 = 0f;
        float newinputSteer2 = 0f;
        float newinputSteer3 = 0f;
        float newinputSteer4 = 0f;
        float newinputSteer5 = 0f;
        float newinputSteer6 = 0f;

        // Drawing Rays.
        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(25, transform.up) * forward * wideRayLength, Color.white);
        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-25, transform.up) * forward * wideRayLength, Color.white);

        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(7, transform.up) * forward * tightRayLength, Color.white);
        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-7, transform.up) * forward * tightRayLength, Color.white);

        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(90, transform.up) * forward * sideRayLength, Color.white);
        Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-90, transform.up) * forward * sideRayLength, Color.white);

        // Wide Raycasts.
        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(25, transform.up) * forward, out hit, wideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(25, transform.up) * forward * wideRayLength, Color.red);
            newinputSteer1 = Mathf.Lerp(-.5f, 0f, (hit.distance / wideRayLength));
            wideTurn = true;
        }

        else
        {
            newinputSteer1 = 0f;
            wideTurn = false;
        }

        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(-25, transform.up) * forward, out hit, wideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-25, transform.up) * forward * wideRayLength, Color.red);
            newinputSteer4 = Mathf.Lerp(.5f, 0f, (hit.distance / wideRayLength));
            wideTurn1 = true;
        }
        else
        {
            newinputSteer4 = 0f;
            wideTurn1 = false;
        }

        // Tight Raycasts.
        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(7, transform.up) * forward, out hit, tightRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(7, transform.up) * forward * tightRayLength, Color.red);
            newinputSteer3 = Mathf.Lerp(-1f, 0f, (hit.distance / tightRayLength));
            tightTurn = true;
        }
        else
        {
            newinputSteer3 = 0f;
            tightTurn = false;
        }

        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(-7, transform.up) * forward, out hit, tightRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-7, transform.up) * forward * tightRayLength, Color.red);
            newinputSteer2 = Mathf.Lerp(1f, 0f, (hit.distance / tightRayLength));
            tightTurn1 = true;
        }
        else
        {
            newinputSteer2 = 0f;
            tightTurn1 = false;
        }

        // Side Raycasts.
        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(90, transform.up) * forward, out hit, sideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(90, transform.up) * forward * sideRayLength, Color.red);
            newinputSteer5 = Mathf.Lerp(-1f, 0f, (hit.distance / sideRayLength));
            sideTurn = true;
        }
        else
        {
            newinputSteer5 = 0f;
            sideTurn = false;
        }

        if (Physics.Raycast(pivotPos, Quaternion.AngleAxis(-90, transform.up) * forward, out hit, sideRayLength, obstacleLayers) && !hit.collider.isTrigger && hit.transform.root != transform)
        {
            Debug.DrawRay(pivotPos, Quaternion.AngleAxis(-90, transform.up) * forward * sideRayLength, Color.red);
            newinputSteer6 = Mathf.Lerp(1f, 0f, (hit.distance / sideRayLength));
            sideTurn1 = true;
        }
        else
        {
            newinputSteer6 = 0f;
            sideTurn1 = false;
        }

        if (wideTurn || wideTurn1 || tightTurn || tightTurn1 || sideTurn || sideTurn1)
            raycasting = true;
        else
            raycasting = false;

        if (raycasting)
            rayInput = (newinputSteer1 + newinputSteer2 + newinputSteer3 + newinputSteer4 + newinputSteer5 + newinputSteer6);
        else
            rayInput = 0f;



    }

}
