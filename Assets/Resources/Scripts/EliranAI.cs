﻿using UnityEngine;
using System.Collections;

public class EliranAI : MonoBehaviour {
    RCC_CarControllerV3 vp;
    public RCC_CarControllerV3 pvp; //player vehicle parent
    bool brakeAxis;
    public float HorizontalDisFromPlayer; //leftRight
    public float VerticalDisFromPlayer;//FrontBack
    public Transform target;
    public Transform PlayerTranform;
    bool wait = false;
    public float checkTime = 0.3f;
    float currentCheckTime = 0;
    Rigidbody rb;
    bool approach = false;
    public float coolDownTime = 3f;
    float currentCoolDownTime = 0f;
    int j = 0; //steering switcher
   int f = 0; //fighting switcher
    public float GiveTimeToApproach = 0;
    public float keepBehindDistance = 1f;
    public float KeepSideDistance = 6f;
    float steerVol = 0.1f; //steer straignt
    float temp;
    int ManageSides = 0; //which side the car should go
    [HideInInspector]
    public bool isFree = true;
    public bool isComingFromFront = false;
    public bool avoid = false;
    /* Debug */
    public float breakVol = 0;
    public Transform steerTarget;
    public float SteerTargetDis;
    public float dist;
    public int i = 0; //speed switcher
    public int speed = 0;
    public int playerSpeed = 0;
    public bool InBound = false;
    // Use this for initialization
    void Start () {
        steerTarget = transform.Find("LookAt");
        vp = GetComponent<RCC_CarControllerV3>();
        GameObject t = GameObject.FindGameObjectWithTag("MainPlayer");
        PlayerTranform = t.GetComponent<Transform>();
        pvp = t.GetComponent<RCC_CarControllerV3>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //cc.gasInput = 1f;
        HorizontalDisFromPlayer = transform.position.x - target.position.x;
        Vector3 toTarget = (target.position - transform.position).normalized; // check if next target is infront or back
        VerticalDisFromPlayer = (Vector3.Distance(target.position, transform.position)) * Mathf.Sign(Vector3.Dot(toTarget, transform.forward));
        SteerTargetDis = Vector3.Distance(new Vector3(target.position.x, target.position.y, 0), new Vector3(transform.position.x, transform.position.y, 0)) / 30f;
        speedingConditions();
        //speeding switch control
        switch (i)
        {
            case 0: //ideal / next to player 
                if (isComingFromFront)
                {
                    vp.gasInput = 0f;
                    vp.brakeInput = 1f;
                }
                else {
                    vp.brakeInput = 0f;
                    if (vp.speed > pvp.speed)
                    {
                        vp.gasInput = 0.6f;
                        //      vp.SetBrake(0.01f);
                    }
                    else
                        vp.gasInput = 1f;

                    //      vp.SetBrake(0f);
                    vp.boostInput = 0;
                  
                }
                break;
            case 1: //overpass
                if ((vp.speed > pvp.speed)) /*&& (VerticalDisFromPlayer > keepBehindDistance + 5f)) || isComingFromFront)*/
                {
                    
                    vp.brakeInput = breakVol;
                    //breakVol = 4f;
                    vp.gasInput =0;
                 }
                else {
                    breakVol = 0;
                    Debug.Log("resume speed");
                    vp.gasInput = .2f;
                 }

                break;
            case 2: //over over pass
                breakVol = 0;
                if (vp.speed > pvp.speed || isComingFromFront)
                {
                    vp.gasInput = 0f;
                    vp.brakeInput = 1f;
                    //      clampSpeed = 150f;

                }
                else {
                    vp.gasInput = 1f;
                }

                break;
            case 3:// behind
                breakVol = 0;
                if (isComingFromFront)
                {
                   // AA.enabled = true; match to realistic car controller
                    // autos.enabled = true;
                    isComingFromFront = false;
                }
                vp.brakeInput = 0f;
                vp.gasInput = 1f;
                //Debug.Log(vp.accelInput);
                //  rb.drag = 0f;
                //  clampSpeed = 210f;
                if (vp.speed - 10 > pvp.speed)
                {
                    vp.gasInput = 0f;

                }
                else
                {
                    vp.gasInput = 1f;
                    vp.boostInput = 1f;

                }

                break;
            case 4:// real behind

                breakVol = 0;
                isComingFromFront = false;
                vp.gasInput = 1f;
                 vp.boostInput = 1f;
                vp.brakeInput = 0f;
                //  clampSpeed = 210f;
                break;
            case 5: //ideal / next to player 
                    // vp.SetAccel(0.65f);
                vp.brakeInput = 0f;
                breakVol = 0;
                rb.drag = 0f;
                if (isComingFromFront)
                    vp.gasInput = 0f;
                else
                    vp.gasInput = 0.5f;
                vp.boostInput = 0f;
                Invoke("Approaching", GiveTimeToApproach);
                break;
        }

        //Add Confinement to place where he only steers as player
        if (!avoid)
        {
            if ((currentCheckTime < checkTime && currentCheckTime >= 0) || (SteerTargetDis < 0.03f))
            {
                if (i == 2 || i == 4)
                    vp.steerInput = Mathf.Clamp(Mathf.Lerp(vp.steerInput, Vector3.Cross(transform.forward, Vector3.forward).y, 1f), -SteerTargetDis, SteerTargetDis);
                else
                    vp.steerInput = Mathf.Clamp(Mathf.Lerp(vp.steerInput, Vector3.Cross(transform.forward, PlayerTranform.forward).y, 1f), -SteerTargetDis, SteerTargetDis);
                currentCheckTime += Time.deltaTime;
            }
            else
            {
                if (currentCheckTime > checkTime)
                {
                    Invoke("Approaching", checkTime / 2);
                    currentCheckTime = -1;

                }
                vp.steerInput = Mathf.Clamp(Mathf.Lerp(vp.steerInput, Vector3.Cross(transform.forward, steerTarget.forward).y, 1f), -SteerTargetDis, SteerTargetDis);


            }
        }


       speed = (int)vp.speed;
        playerSpeed = (int)pvp.speed;
    }
    void OnTriggerEnter(Collider other)
    {
        InBound = true;
    }
    void OnTriggerExit(Collider other)
    {
        InBound = false;
    }

    void speedingConditions()
    {
/*        if (approach)
        {
            i = 5;
        }*/

        if (approach || !approach)
        {
            if (VerticalDisFromPlayer > 0 && VerticalDisFromPlayer < keepBehindDistance) // ideal 
            {
                /* if (vp.localVelocity.z > pvp.localVelocity.z)
                 {
                     vp.SetBrake(1f);
                     i = 5;
                 }
                 else {*/
                i = 0;
                approach = true;
            }


            if (VerticalDisFromPlayer < 0)//break
            {
                if (VerticalDisFromPlayer < -50f)
                {
                    i = 2;
                    approach = true;
                }
                else {
                    i = 1;
                    approach = true;

                }

            }

            if (VerticalDisFromPlayer > keepBehindDistance)
            {


                i = 3;
                approach = true;

            }
            if (VerticalDisFromPlayer > keepBehindDistance + 30f)
            {

                i = 4;
                approach = true;
            }
        }
    }
    void Approaching()
    {
        currentCheckTime = 0;
    }

}
