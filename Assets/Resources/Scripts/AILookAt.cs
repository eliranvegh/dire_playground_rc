﻿using UnityEngine;
using System.Collections;

public class AILookAt : MonoBehaviour {
    public Transform target;
	// Use this for initialization
	void Start () {
        target = GetComponentInParent<EliranAI>().target;
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(target);
    }
}
